﻿using AnnuaireAgroFood.ViewModel;
using System.Windows;

namespace AnnuaireAgroFood {
    public partial class CreateEmployeeWindow : Window {

        public MainViewModel _viewModel = new MainViewModel();

        public CreateEmployeeWindow() {
            InitializeComponent();
            this.DataContext = _viewModel;
        }

        private void BtAddEmployee_Click(object sender, RoutedEventArgs e) {
            _viewModel.AddEmployee();
            Close();
        }
    }
}
