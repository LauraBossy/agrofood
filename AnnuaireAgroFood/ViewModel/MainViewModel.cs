﻿using AnnuaireAgroFood.Models;
using CommunityToolkit.Mvvm.ComponentModel;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows;

namespace AnnuaireAgroFood.ViewModel
{
    public class MainViewModel : ObservableObject
    {

        // Je spécifie quel sera le modèle
        private IEmployeesDataSource _model = new EmployeeDataSource();

        // Les attributs
        private ObservableCollection<Employee> _lstWithAllEmployees;
        private ObservableCollection<Employee> _lstModified;
        private string _filter;
        private bool _isAdmin = false;
        private Employee _employee = new Employee();

        // Les propriétés
        public ObservableCollection<Employee> LstModified
        {
            get => _lstModified;
            set => SetProperty(ref _lstModified, value);
        }
        public string Filter
        {
            get => _filter;
            set
            {
                _filter = value;
                OnPropertyChanged("Filter");
                UpdateListWithFilter();
            }
        }
        public bool IsAdmin
        {
            get => _isAdmin;
            set => SetProperty(ref _isAdmin, value);
        }

        public Employee Employee
        {
            get => _employee;
            set => _employee = value;
        }

        public Employee SelectedEmployee { get; set; }

        public bool IsThereOneEmployeeSelected
        {
            get => SelectedEmployee != null;
        }

        // Le constructeur
        public MainViewModel()
        {
            // J'initialise _lstWithAllEmployees
            _lstWithAllEmployees = _model.GetAll();
            // J'initialise LstModified
            LstModified = new ObservableCollection<Employee>(_lstWithAllEmployees);
        }

        // Les méthodes
        public void BecomeAdmin()
        {
            IsAdmin = true;
        }

        public void StopAdmin()
        {
            IsAdmin = false;
        }

        public void DeleteSelectedEmployee()
        {
            _model.Delete(SelectedEmployee.Id);
            _lstWithAllEmployees = _model.GetAll();
            LstModified = new ObservableCollection<Employee>(_lstWithAllEmployees);
        }

        public void AddEmployee()
        {
            _model.Create(Employee);
            _lstWithAllEmployees = _model.GetAll();
            LstModified = new ObservableCollection<Employee>(_lstWithAllEmployees);
        }

        public void UpdateEmployee()
        {
            //_model.Update(Employee);
            //_lstWithAllEmployees = _model.GetAll();
            //LstModified = new ObservableCollection<Employee>(_lstWithAllEmployees);
        }

        private bool hasEmployeeFilter(Employee e, string filter)
        {
            filter = filter.ToLower();
            if (
                (e.Name.ToLower()).Contains(filter) ||
                (e.Firstname.ToLower()).Contains(filter) ||
                (e.Tel.ToLower()).Contains(filter) ||
                (e.Email.ToLower()).Contains(filter) ||
                (e.Service.ToLower()).Contains(filter) ||
                (e.Site.ToLower()).Contains(filter)
            )
            {
                return true;
            }
            return false;
        }

        private bool hasEmployeeAllFilters(Employee e, string[] filters)
        {
            foreach (string f in filters)
            {
                if (!hasEmployeeFilter(e, f))
                {
                    return false;
                }
            }
            return true;
        }

        private void UpdateListWithFilter()
        {
            LstModified.Clear();
            string[] Filters = Filter.Trim().Split(' ');

            foreach (Employee e in _lstWithAllEmployees)
            {
                if (hasEmployeeAllFilters(e, Filters))
                {
                    LstModified.Add(e);
                }
            }
        }
    }
}
