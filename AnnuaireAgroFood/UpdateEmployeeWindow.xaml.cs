﻿using AnnuaireAgroFood.ViewModel;
using System.Windows;

namespace AnnuaireAgroFood {
    public partial class UpdateEmployeeWindow : Window {

        public MainViewModel _viewModel = new MainViewModel();

        public UpdateEmployeeWindow() {
            InitializeComponent();
        }

        private void BtUpdateEmployee_Click(object sender, RoutedEventArgs e) {
            _viewModel.UpdateEmployee();
        }
    }
}
