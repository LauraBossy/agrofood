﻿namespace AnnuaireAgroFood.Models {

    // La classe Employees : tout ce qu'un employee possède comme attribut

    public class Employee {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Firstname { get; set; }
        public string Tel { get; set; }
        public string Email { get; set; }
        public string Service { get; set; }
        public string Site { get; set; }

    }
}
