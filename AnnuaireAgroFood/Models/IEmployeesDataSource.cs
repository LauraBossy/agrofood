﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace AnnuaireAgroFood.Models {

    // Le contrat : la liste des choses qu'il faudra implémenter

    interface IEmployeesDataSource {

        ObservableCollection<Employee> GetAll();

        void Create(Employee e);

        void Update(Employee e);

        void Delete(int employeeId);

    }
}
