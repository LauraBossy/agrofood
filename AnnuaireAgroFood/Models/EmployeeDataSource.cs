﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;

namespace AnnuaireAgroFood.Models {

    // L'implémentation du contrat d'interface

    public class EmployeeDataSource : IEmployeesDataSource {

        private MySqlConnection _connection = new MySqlConnection("server=localhost;database=agrofood;uid=root;password=;");

        public MySqlConnection Connection {
            get {
                return _connection; 
            }
        }

        public ObservableCollection<Employee> GetAll() {

            DataTable dt = new DataTable();

            string query = "SELECT `id`, `name`, `firstname`, `tel`, `email`, `service`, `site` FROM employees";

            MySqlCommand cmd = new MySqlCommand(query, Connection);

            Connection.Open();
            dt.Load(cmd.ExecuteReader());
            Connection.Close();

            var lstEmployees = new ObservableCollection<Employee>();

            for (int i = 0; i < dt.Rows.Count; i++) {
                Employee employee = new Employee();
                employee.Id = Convert.ToInt32(dt.Rows[i]["id"]);
                employee.Name = dt.Rows[i]["name"].ToString();
                employee.Firstname = dt.Rows[i]["firstname"].ToString();
                employee.Tel = dt.Rows[i]["tel"].ToString();
                employee.Email = dt.Rows[i]["email"].ToString();
                employee.Service = dt.Rows[i]["service"].ToString();
                employee.Site = dt.Rows[i]["site"].ToString();
                lstEmployees.Add(employee);
            }

            return lstEmployees;

        }

        public void Create(Employee e) {            
            string name = e.Name;
            string firstname = e.Firstname;
            string tel = e.Tel;
            string email = e.Email;
            string service = e.Service;
            string site = e.Site;
            string query = "INSERT INTO employees (name, firstname, tel, email, service, site) " +
                           "VALUES ('" + name + "', '" + firstname + "', '" + tel + "', '" + email + "', '" + service + "', '" + site + "')";

            MySqlCommand cmd = new MySqlCommand(query, Connection);

            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        public void Update(Employee e) {
            int id = e.Id;
            string name = e.Name;
            string firstname = e.Firstname;
            string tel = e.Tel;
            string email = e.Email;
            string service = e.Service;
            string site = e.Site;

            string query = "UPDATE employees SET " +
                "`name` = '" + name + "', " +
                "`firstname` = '" + firstname + "', " +
                "`tel`  = '" + tel + "', " +
                "`email`  = '" + email + "', " +
                "`service`  = '" + service + "', " +
                "`site`  = '" + site + "', " +
                "where `id` = " + id + ";";

            MySqlCommand cmd = new MySqlCommand(query, Connection);

            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }

        public void Delete(int employeeId) {
            string query = "DELETE FROM employees WHERE `id` = " + employeeId + ";";
            MySqlCommand cmd = new MySqlCommand(query, Connection);

            Connection.Open();
            cmd.ExecuteNonQuery();
            Connection.Close();
        }
    }
}
