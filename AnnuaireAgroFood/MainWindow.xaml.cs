﻿using AnnuaireAgroFood.ViewModel;
using System;
using System.Windows;
using System.Windows.Input;

namespace AnnuaireAgroFood {

    public partial class MainWindow : Window {

        private MainViewModel _viewModel = new MainViewModel();

        public MainWindow() {
            InitializeComponent();
            this.DataContext = _viewModel;
        }

        private void BtOpenCreateEmployeeWindow_Click(object sender, RoutedEventArgs e) {
            var CreateEmployeeWindow = new CreateEmployeeWindow();
            CreateEmployeeWindow.Show();
        }

        private void BtOpenUpdateEmployeeWindow_Click(object sender, RoutedEventArgs e) {
            var UpdateEmployeeWindow = new UpdateEmployeeWindow();
            UpdateEmployeeWindow.Show();
        }

        private void BtDeleteEmployee_CLick(object sender, RoutedEventArgs e) {
            if (_viewModel.IsThereOneEmployeeSelected)
                _viewModel.DeleteSelectedEmployee();
            else
                MessageBox.Show("Veuillez sélectionner un employé");
        }

        private void Admin_Click(object sender, System.Windows.Input.KeyEventArgs e) {
            // Keyboard.Modifiers.HasFlag() est une méthode qui vérifie n'importe quelles touches renseignées (il y'a deux touches "modifiers" chacun pour Ctrl Alt)
            var ctrlIsDown = Keyboard.Modifiers.HasFlag(ModifierKeys.Control);
            var altIsDown = Keyboard.Modifiers.HasFlag(ModifierKeys.Alt);
            var eKeyIsDown = Keyboard.IsKeyDown(Key.E);

            if (ctrlIsDown && altIsDown && eKeyIsDown)
            {
                if (!_viewModel.IsAdmin) {
                    _viewModel.BecomeAdmin();
                }
                else {
                    _viewModel.StopAdmin();
                }
            }
        }
    }
}
